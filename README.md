<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">机器学习的医疗数据</font></font></h1><a id="user-content-medical-data-for-machine-learning" class="anchor" aria-label="永久链接：机器学习的医疗数据" href="#medical-data-for-machine-learning"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这是一份精选的机器学习医疗数据列表。</font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
此列表仅供参考，请确保您遵守此处列出的任何数据的所有使用限制。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1. 医学影像数据</font></font></h2><a id="user-content-1-medical-imaging-data" class="anchor" aria-label="永久链接：1. 医学影像数据" href="#1-medical-imaging-data"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">EchoNet-Dynamic</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
来自斯坦福的用于医学机器学习的大型新型心脏运动视频数据资源。概览：</font></font><a href="https://echonet.github.io/dynamic/index.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://echonet.github.io/dynamic/index.html</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
访问： https: </font></font><a href="https://echonet.github.io/dynamic/index.html#access" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">//echonet.github.io/dynamic/index.html#access</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">美国国家医学图书馆提供 MedPix®</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
数据库，其中包含来自 13,000 名患者的 53,000 张带注释的医学图像。</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">需要注册</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
信息：</font></font><a href="https://medpix.nlm.nih.gov/home" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://medpix.nlm.nih.gov/home</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ABIDE：自闭症大脑成像数据交换：旨在大规模评估自闭症的内在大脑结构。539</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
名患有 ASD 的个体和 573 名典型对照者的功能 MRI 图像。这 1112 个数据集由结构和静息状态功能 MRI 数据以及大量表型信息组成。</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">需要注册</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
论文：</font></font><a href="http://www.ncbi.nlm.nih.gov/pubmed/23774715" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.ncbi.nlm.nih.gov/pubmed/23774715</font></font></a><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
信息： http: </font></font><a href="http://fcon_1000.projects.nitrc.org/indi/abide/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">//fcon_1000.projects.nitrc.org/indi/abide/</font></font></a><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
预处理版本：http: </font></font><a href="http://preprocessed-connectomes-project.org/abide/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">//preprocessed-connectomes-project.org/abide/</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阿尔茨海默病神经影像学倡议 (ADNI)</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
的阿尔茨海默病患者和健康对照者的 MRI 数据库。还包含临床、基因组和生物标记物数据。</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">需要注册</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
论文：</font></font><a href="http://www.neurology.org/content/74/3/201.short" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.neurology.org/content/74/3/201.short</font></font></a><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
访问：</font></font><a href="http://adni.loni.usc.edu/data-samples/access-data/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://adni.loni.usc.edu/data-samples/access-data/</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">结肠癌 CT 结肠造影术（癌症影像档案）</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
用于诊断结肠癌的 CT 扫描。包括无息肉、6-9 毫米息肉和大于 10 毫米息肉患者的数据。访问：</font></font><a href="https://wiki.cancerimagingarchive.net/display/Public/CT+COLONOGRAPHY#dc149b9170f54aa29e88f1119e25ba3e" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://wiki.cancerimagingarchive.net/display/Public/CT+COLONOGRAPHY#dc149b9170f54aa29e88f1119e25ba3e</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于血管提取的数字视网膜图像 (DRIVE)</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> 
DRIVE 数据库用于视网膜图像中血管分割的比较研究。它包含 40 张照片，其中 7 张显示出轻微早期糖尿病视网膜病变的迹象。</font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
论文： https: </font></font><a href="https://ieeexplore.ieee.org/document/1282003" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">//ieeexplore.ieee.org/document/1282003</font></font></a><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
访问：</font></font><a href="http://www.isi.uu.nl/Research/Databases/DRIVE/download.php" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.isi.uu.nl/Research/Databases/DRIVE/download.php</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SynthStrip</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> 
SynthStrip 数据集是经过许可的全头部图像和真实脑部掩模的集合，来自 600 多张 MRI、CT 和 PET 扫描。它包括一系列具有不同对比度、分辨率和受试者群体的 MRI 扫描，受试者范围从婴儿到胶质母细胞瘤患者。该数据集可用于开发和评估脑提取（或颅骨剥离）算法。</font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
论文：</font></font><a href="https://doi.org/10.1016/j.neuroimage.2022.119474" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://doi.org/10.1016/j.neuroimage.2022.119474</font></font></a><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
访问：</font></font><a href="https://w3id.org/synthstrip" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://w3id.org/synthstrip</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AMRG 心脏图谱</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
AMRG 心脏 MRI 图谱是使用奥克兰 MRI 研究小组的西门子 Avanto 扫描仪获取的正常患者心脏的完整标记 MRI 图像集。该图谱旨在为大学和学校学生、MR 技术人员、临床医生提供...</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">先天性心脏病 (CHD) 图集</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
先天性心脏病 (CHD) 图集代表患有各种先天性心脏缺陷的成人和儿童的 MRI 数据集、生理临床数据和计算机模型。数据来自包括 Rady 在内的多家临床中心...</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过磁共振成像评估确定</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
除颤器降低风险的方法，是一项针对冠状动脉疾病和轻度至中度左心室功能障碍患者的前瞻性、多中心、随机临床试验。主要目的...</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MESA</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
动脉粥样硬化多民族研究是一项在美国六个中心进行的大规模心血管人群研究（&gt;6,500 名参与者）。该研究旨在调查亚临床至临床心血管疾病在发病前的表现...</font></font></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OASIS</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
开放获取影像研究系列 (OASIS) 是一个旨在向科学界免费提供大脑 MRI 数据集的项目。目前有两个数据集可用：一个是横断面数据集，另一个是纵向数据集。</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">年轻人、中年人、非痴呆和痴呆老年人的横断面 MRI 数据：该数据集由 416 名年龄在 18 至 96 岁之间的受试者的横断面集合组成。对于每个受试者，包括在单次扫描会话中获得的 3 或 4 次单独的 T1 加权 MRI 扫描。受试者都是右撇子，包括男性和女性。其中 100 名 60 岁以上的受试者被临床诊断患有极轻度至中度阿尔茨海默病 (AD)。此外，还包括一个可靠性数据集，其中包含 20 名非痴呆受试者，这些受试者在首次会话后 90 天内的后续访问中进行了成像。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">非痴呆和痴呆老年人的纵向 MRI 数据：该组数据由 150 名年龄在 60 至 96 岁之间的受试者的纵向集合组成。每位受试者接受两次或多次扫描，间隔至少一年，总共进行 373 次成像。对于每位受试者，包括在单次扫描中获得的 3 或 4 次单独的 T1 加权 MRI 扫描。受试者都是右撇子，包括男性和女性。在整个研究过程中，72 名受试者被诊断为非痴呆。64 名受试者在首次就诊时被诊断为痴呆，并在后续扫描中保持这种状态，其中包括 51 名患有轻度至中度阿尔茨海默病的个体。另外 14 名受试者在首次就诊时被诊断为非痴呆，随后在后续就诊中被诊断为痴呆。</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="http://www.oasis-brains.org/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.oasis-brains.org/</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Isic 档案 - 黑色素瘤</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
该档案包含 23k 张分类皮肤病变图像。其中包含恶性和良性样本。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">每个示例包含病变的图像、有关病变的元数据（包括分类和分割）以及有关患者的元数据。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">可以在以下链接中查看数据：</font></font><a href="https://www.isic-archive.com" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https：//www.isic-archive.com</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（在图库部分）</font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
可以通过网站或使用此存储库下载：</font></font><br>
<a href="https://github.com/GalAvineri/ISIC-Archive-Downloader"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https：//github.com/GalAvineri/ISIC-Archive-Downloader</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SCMR 共识数据</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
SCMR 共识数据集是一组 15 项混合病理心脏 MRI 研究（5 项健康、6 项心肌梗塞、2 项心力衰竭和 2 项肥大），这些研究来自不同的 MR 机器（4 台 GE、5 台西门子、6 台飞利浦）。主要目标...</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Sunnybrook 心脏数据</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
Sunnybrook 心脏数据 (SCD)，也称为 2009 年心脏 MR 左心室分割挑战数据，包含来自不同患者和病理的 45 张电影 MRI 图像：健康、肥大、伴有梗塞的心力衰竭和心脏...</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="http://www.cardiacatlas.org/studies/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.cardiacatlas.org/studies/</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">肺部图像数据库联盟 (LIDC)</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">初步临床研究表明，螺旋 CT 肺部扫描可提高高危人群肺癌的早期检测率。图像处理算法有可能协助螺旋 CT 研究中病变的检测，并评估连续 CT 研究中病变大小的稳定性或变化。使用此类计算机辅助算法可以显著提高螺旋 CT 肺部筛查的灵敏度和特异性，并通过减少医生解释所需的时间来降低成本。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">肺成像数据库联盟 (LIDC) 计划旨在支持一个机构联盟制定螺旋 CT 肺图像资源的共识指南并构建螺旋 CT 肺图像数据库。该计划资助的研究人员创建了一套数据库使用指南和指标，并开发了一个数据库作为这些方法的试验台和展示平台。该数据库可通过互联网供研究人员和用户使用，并可作为研究、教学和培训资源广泛使用。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">具体来说，LIDC 计划旨在提供：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于图像处理或 CAD 算法相对评估的参考数据库和</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">一个灵活的查询系统，将为研究人员提供机会评估该数据库内可能对研究应用很重要的广泛技术参数和匿名临床信息。</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">该资源将促进图像处理和 CAD 评估数据库的进一步开发，以用于癌症筛查、诊断、图像引导干预和治疗等应用。因此，NCI 鼓励研究人员发起的资助申请，在其研究中使用该数据库。NCI 还鼓励研究人员发起的资助申请，提供可能改善或补充 LIDC 使命的工具或方法。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="https://wiki.cancerimagingarchive.net/display/Public/LIDC-IDRI#" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https：//wiki.cancerimagingarchive.net/display/Public/LIDC-IDRI#</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TCIA 收藏品</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">癌症成像数据集涵盖各种癌症类型（例如癌、肺癌、骨髓瘤）和各种成像方式。癌症成像档案 (TCIA) 中的图像数据被组织成专门构建的主题集合。这些主题通常具有共同的癌症类型和/或解剖部位（肺、脑等）。下表中的每个链接都包含有关集合的科学价值的信息、有关如何获取任何可能可用的支持非图像数据的信息以及查看或下载图像数据的链接。为了支持科学研究的可重复性，TCIA 支持数字对象标识符 (DOI)，允许用户共享研究手稿中引用的 TCIA 数据子集。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="http://www.cancerimagingarchive.net/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.cancerimagingarchive.net/</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">白俄罗斯结核病门户网站</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">结核病 (TB) 是白俄罗斯公共卫生的主要问题。最近，MDR/XDR TB 和 HIV/TB 的出现和发展使情况变得复杂，需要长期治疗。许多最严重的病例通常会传播到全国各地的不同结核病诊所。使用包含患者放射图像、实验室工作和临床数据的通用数据库，白俄罗斯主要结核病专家跟踪此类患者的能力将大大提高。这也将显著提高对治疗方案的依从性，并更好地记录治疗结果。门户网站数据库的纳入临床病例标准 - 入住 RSPC 肺病和结核病 MDR-TB 科室并被诊断或怀疑患有 MDR-TB 的患者，进行了 CT 检查（从登记之日起±2 个月）白俄罗斯数据集包含同一患者的胸部 X 光和 CT 扫描。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="http://tuberculosis.by/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://tuberculosis.by/</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DDSM：乳房X光检查数字数据库</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">筛查乳房 X 线摄影数字数据库 (DDSM) 是供乳房 X 线摄影图像分析研究社区使用的资源。该项目的主要资助来自美国陆军医学研究与物资司令部乳腺癌研究计划的资助。DDSM 项目是麻省总医院 (D. Kopans、R. Moore)、南佛罗里达大学 (K. Bowyer) 和桑迪亚国家实验室 (P. Kegelmeyer) 的共同研究项目。华盛顿大学医学院的其他病例由放射学和内科助理教授 Peter E. Shile 医学博士提供。其他合作机构包括维克森林大学医学院 (医学工程系和放射学系)、圣心医院和 ISMD, Incorporated。该数据库的主要目的是促进开发计算机算法以帮助筛查的合理研究。该数据库的次要目的可能包括开发算法以帮助诊断和开发教学或培训辅助工具。该数据库包含大约 2,500 项研究。每项研究包括每只乳房的两张图像，以及一些相关的患者信息（研究时的年龄、ACR 乳房密度评级、异常的细微度评级、ACR 异常的关键字描述）和图像信息（扫描仪、空间分辨率等）。包含可疑区域的图像具有与可疑区域的位置和类型相关的像素级“地面真实”信息。还提供了用于访问乳房 X 光检查和真实图像以及计算自动图像分析算法性能数据的软件。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="http://marathon.csee.usf.edu/Mammography/Database.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://marathon.csee.usf.edu/Mammography/Database.html</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">INbreast：数字乳房 X 线摄影数据库</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">INbreast 数据库是一个乳房 X 线摄影数据库，其图像是在大学医院（Hospital de São João，乳腺中心，波尔图，葡萄牙）的乳腺中心获取的。INbreast 共有 115 个病例（410 张图像），其中 90 个病例来自双乳女性（每个病例 4 张图像），25 个病例来自乳房切除术患者（每个病例 2 张图像）。其中包括几种类型的病变（肿块、钙化、不对称和扭曲）。专家绘制的精确轮廓也以 XML 格式提供。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="http://medicalresearch.inescporto.pt/breastresearch/index.php/Get_INbreast_Database" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://medicalresearch.inescporto.pt/breastresearch/index.php/Get_INbreast_Database</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">mini-MIAS：MIAS MiniMammographic 数据库</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">乳房 X 线摄影图像分析协会 (MIAS) 是由英国研究小组组成的组织，致力于了解乳房 X 线摄影，并已生成数字乳房 X 线摄影数据库。英国国家乳房筛查计划拍摄的胶片已使用 Joyce-Loebl 扫描微密度计数字化为 50 微米像素边缘，该设备在光密度范围 0-3.2 内呈线性，每个像素用 8 位字表示。该数据库包含 322 张数字化胶片，可在 2.3GB 8mm（ExaByte）磁带上获取。它还包括放射科医生对可能存在的任何异常位置的“真实”标记。数据库已缩小到 200 微米像素边缘并进行了填充/剪切，因此所有图像均为 1024x1024。乳房 X 线摄影图像可通过埃塞克斯大学的试点欧洲图像处理档案 (PEIPA) 获取。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="http://peipa.essex.ac.uk/info/mias.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://peipa.essex.ac.uk/info/mias.html</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">前列腺</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">前列腺癌（CaP）是全球范围内男性中第二大最常见的癌症，占13.6％（Ferlay等（2010））。据统计，2008年新诊断病例数估计为899,000例，死亡人数不少于258,100例（Ferlay等（2010））。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">磁共振成像 (MRI) 提供可用于诊断和定位 CaP 的成像技术。I2CVB 提供多参数 MRI 数据集，以帮助开发计算机辅助检测和诊断 (CAD) 系统。访问：</font></font><a href="http://i2cvb.github.io/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://i2cvb.github.io/</font></font></a></p>
<hr>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="http://www.ehealthlab.cs.ucy.ac.cy/index.php/facilities/32-software/218-datasets" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.ehealthlab.cs.ucy.ac.cy/index.php/facilities/32-software/218-datasets</font></font></a></p>
<ul dir="auto">
<li>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">多发性硬化症数据库中的 MRI 病变分割</font></font></strong></p>
</li>
<li>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">紧急远程骨科X射线数字图书馆</font></font></strong></p>
</li>
<li>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">IMT 分割</font></font></strong></p>
</li>
<li>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">针状 EMG MUAP 时间域特征</font></font></strong></p>
</li>
</ul>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DICOM 图像样本集</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
这些数据集仅供研究和教学使用。您无权重新分发或出售它们，或将它们用于商业目的。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">所有这些 DICOM 文件都采用 JPEG2000 传输语法压缩。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="http://www.osirix-viewer.com/resources/dicom-image-library/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http：//www.osirix-viewer.com/resources/dicom-image-library/</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SCR 数据库：胸部 X 光片中的分割</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">胸部X光片中解剖结构的自动分割对于计算机辅助诊断具有重要意义。SCR数据库的建立是为了便于对标准前后胸部X光片中肺部、心脏和锁骨的分割进行比较研究。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">本着合作科学进步的精神，我们免费共享 SCR 数据库，并致力于维护一个公开的存储库，其中包含这些分割任务中各种算法的结果。在这些页面上，可以找到有关下载数据库和上传结果的说明，并且可以检查各种方法的基准结果。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="http://www.isi.uu.nl/Research/Databases/SCR/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.isi.uu.nl/Research/Databases/SCR/</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医学图像数据库和图书馆</font></font></strong></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="http://www.omnimedicalsearch.com/image_databases.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.omnimedicalsearch.com/image_databases.html</font></font></a></strong></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">普通类别</font></font></strong></p>
<ul dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">e-Anatomy.org - 交互式解剖图谱 - e-anatomy 是一个解剖学电子学习网站。选择了 1500 多张来自正常 CT 和 MR 检查的切片，以覆盖人体的整个断层解剖结构。图像使用 Terminologia Anatomica 进行标记。用户友好的界面允许通过多切片图像系列与交互式文本信息、3D 模型和解剖图进行电影放映。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医学图片和定义 - 欢迎来到互联网上最大的医学图片和定义数据库。提供医学信息的网站很多，但提供医学图片的网站却很少。据我们所知，我们是唯一一家提供医学图片数据库的网站，其中包含图片中每个术语的基本信息。编者注：不错的网站，免费访问，无需注册即可访问 1200 多张健康和医学相关图片，并附有定义。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nucleus Medical Art - 医学插图，医学艺术。包括 3D 动画。“Nucleus Medical Art, Inc. 是医学插图、医学动画和交互式多媒体的领先创作者和分销商，服务于美国和国外的出版、法律、医疗保健、娱乐、制药、医疗设备、学术界和其他市场。编者注：很棒的网站。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">互联网上的医学图像数据库 (UTHSCSA 图书馆) - 包含特定主题医学相关图像的网站链接目录。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">手术视频 - 美国国家医学图书馆 MedlinePlus 收集了数百种不同手术程序的链接。您必须在计算机上安装 RealPlayer 媒体播放器才能观看这些免费视频。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">带插图的 ADAM 医学百科全书。ADAM 医学百科全书可能是当今互联网上带插图的最佳医学著作之一，它包含 4,000 多篇有关疾病、检查、症状、伤害和手术的文章。它还包含一个庞大的医学照片和插图库来支持这 4,000 篇文章。这些插图和文章免费向公众开放。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Hardin MD - 医学和疾病图片，是爱荷华大学提供的免费和成熟的资源，已有相当长一段时间。主页采用目录形式，用户必须深入查找他们要查找的图像，其中许多图像不在网站上。尽管如此，Hardin MD 仍然是通往数千张详细医学照片和插图的绝佳门户。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">健康教育资产库 (HEAL) - 互联网健康基金会媒体库 总部位于瑞士的 (HON) 是一家国际机构，致力于鼓励以合乎道德的方式提供在线健康信息。 “HONmedia（图片库）是一个独特的存储库，包含 6,800 多张医学图片和视频，涉及 1,700 个主题和主题。这个无与伦比的数据库由 HON 手动创建，并不断从万维网上添加新的图片链接。HON 鼓励用户通过提交图片链接提供自己的图片链接。” 图书馆包括解剖图像、疾病和病症及程序的视觉效果。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">公共卫生图像库 (PHIL) 由美国疾病控制与预防中心 (CDC) 的一个工作组创建，PHIL 为 CDC 的图片提供了一个有组织的通用电子门户。我们欢迎公共卫生专业人员、媒体、实验室科学家、教育工作者、学生和全球公众使用此资料作为参考、教学、演示和公共卫生信息。内容按人物、地点和科学的层次分类组织，并以单张图片、图片集和多媒体文件的形式呈现。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医学史图片 - 该系统提供对美国国家医学图书馆 (NLM) 医学史部 (HMD) 印刷品和照片收藏中近 60,000 幅图片的访问。该收藏包括肖像、机构图片、漫画、风俗画和各种媒体的平面艺术，展示了医学的社会和历史方面。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Pozemedicale.org - 收集西班牙语、意大利语、葡萄牙语和意大利语的医学图像。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">古老的医学图片：数百张来自 19 世纪末和 20 世纪初的令人着迷、有趣但高质量的古老照片和图像。</font></font></p>
</li>
</ul>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">主题专业图像库和收藏</font></font></strong></p>
<ul dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">亨利·格雷的《人体解剖学》—— Bartleby.com 版的格雷《人体解剖学》收录了 1,247 幅色彩鲜艳的版画，其中许多都是彩色的，均出自 1918 年的经典出版物。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">克鲁克斯顿收藏 - 约翰·H·克鲁克斯顿博士拍摄的医学幻灯片集，已数字化并可供公众和医生使用。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DAVE 项目 - 一个可搜索的胃肠内窥镜视频剪辑库，涵盖广泛的内窥镜成像。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Dermnet - 可浏览超过 8,000 个高质量皮肤病学图像。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">交互式皮肤病学图谱 - 常见和不常见皮肤问题的图像参考来源。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">多维人类胚胎是美国国家儿童健康与人类发展研究所 (NICHD) 资助的一项合作项目，旨在制作并通过互联网提供基于磁共振成像的人类胚胎三维图像参考。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GastroLab 内窥镜档案于 1996 年启动，目标是维护一个内窥镜图像库，供所有感兴趣的医疗保健人员免费使用。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MedPix 是一款放射学和医学图片数据库资源工具。主页界面混乱，整个网站设计不方便用户使用，给人一种 20 世纪 90 年代中期的感觉。但是，如果您有时间（耐心），它可能对某些人来说是一种重要的资源。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OBGYN.net 图片库 - 该网站致力于提供与女性健康相关的图片。除了为您提供 OBGYN.net 图片之外，我们还在互联网上提供其他与女性健康相关的图片。由于这些图片具有图像性质，有些人可能不愿意查看这些图片。这些图片仅供教育之用。</font></font></p>
</li>
</ul>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">VIA 集团公共数据库</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">记录图像数据库对于定量图像分析工具的开发至关重要，尤其是对于计算机辅助诊断 (CAD) 任务。我们与 I-ELCAP 小组合作建立了两个公共图像数据库，其中包含 DICOM 格式的肺部 CT 图像以及放射科医生对异常情况的记录。请访问以下链接了解更多详情：</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="http://www.via.cornell.edu/databases/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.via.cornell.edu/databases/</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CVonline：图像数据库</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
访问：</font></font><a href="http://homepages.inf.ed.ac.uk/rbf/CVonline/Imagedbase.htm" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://homepages.inf.ed.ac.uk/rbf/CVonline/Imagedbase.htm</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">USC-SIPI 图像数据库</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
USC-SIPI 图像数据库是数字化图像的集合。它主要用于支持图像处理、图像分析和机器视觉方面的研究。USC-SIPI 图像数据库的第一版于 1977 年发布，此后又添加了许多新图像。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据库根据图片的基本特征分为几卷。每卷中的图像大小各异，例如 256x256 像素、512x512 像素或 1024x1024 像素。所有图像均为 8 位/像素（黑白图像），24 位/像素（彩色图像）。目前可用的卷如下：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>Textures 	Brodatz textures, texture mosaics, etc.
Aerials 	High altitude aerial images
Miscellaneous 	Lena, the mandrill, and other favorites
Sequences 	Moving head, fly-overs, moving vehicles
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="Textures 	Brodatz textures, texture mosaics, etc.
Aerials 	High altitude aerial images
Miscellaneous 	Lena, the mandrill, and other favorites
Sequences 	Moving head, fly-overs, moving vehicles" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="http://sipi.usc.edu/database/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://sipi.usc.edu/database/</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">组织学数据集：不同染色切片的图像配准</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">该数据集由 2D 组织学显微镜组织切片组成，这些切片用不同的染色剂染色，并有标记每个切片中关键点的标志。任务是图像配准 - 将特定图像集（连续染色切片）中的所有切片对齐在一起，例如对齐到初始图像平面。这些图像面临的主要挑战如下：图像尺寸非常大、外观差异大以及缺乏具有独特外观的物体。该数据集包含 108 个图像对和手动放置的标志，用于配准质量评估。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="http://cmp.felk.cvut.cz/~borovji3/?page=dataset" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://cmp.felk.cvut.cz/~borovji3/ ?page=dataset</font></font></a></p>
<hr>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2. 挑战/比赛数据</font></font></h2><a id="user-content-2-challengescontest-data" class="anchor" aria-label="永久链接：2. 挑战/比赛数据" href="#2-challengescontest-data"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">放射学中的视觉概念提取挑战</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
手动注释来自几种不同成像模式（例如 CT 和 MR）的几种解剖结构（例如肾脏、肺、膀胱等）的放射学数据。他们还提供了一个云计算实例，任何人都可以使用它来开发和评估基准模型。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="http://www.visceral.eu/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.visceral.eu/</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">生物医学图像分析的重大挑战</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">一系列生物医学成像挑战，通过标准化评估标准，便于</font></font><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">更好地比较新旧解决方案</font></font></em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。您也可以创建自己的挑战。截至撰写本文时，已有 92 个挑战提供可下载的数据集。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="http://www.grand-challenge.org/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.grand-challenge.org/</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">梦想挑战</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DREAM 挑战赛提出了有关系统生物学和转化医学的基本问题。我们的挑战赛由来自不同组织的研究人员社区设计和运营，邀请参与者提出解决方案 — 在此过程中促进合作并建立社区。Sage Bionetworks 提供专业知识和机构支持，并通过其 Synapse 平台提供举办挑战赛的基础设施。我们共同拥有一个愿景，即允许个人和团体公开合作，以便“群体智慧”对科学和人类健康产生最大影响。</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数字乳房 X 线摄影梦想挑战赛。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ICGC-TCGA DREAM 体细胞突变调用 RNA 挑战 (SMC-RNA)</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">梦想创意挑战</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这些是添加时活跃的挑战，还有更多过去的挑战和即将到来的挑战！</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="http://dreamchallenges.org/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://dreamchallenges.org/</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Kaggle 糖尿病视网膜病变</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">高分辨率视网膜图像，由临床医生按 0-4 严重程度等级标注，用于检测糖尿病视网膜病变。此数据集是已完成的 Kaggle 竞赛的一部分，该竞赛通常是公开数据集的重要来源。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="https://www.kaggle.com/c/diabetic-retinopathy-detection" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https：//www.kaggle.com/c/diabetic-retinopathy-detection</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">宫颈癌筛查</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在本次 Kaggle 竞赛中，您将开发算法，根据宫颈图像正确分类宫颈类型。我们数据集中的这些不同类型的宫颈均被视为正常（非癌变），但由于转化区并不总是可见，因此有些患者需要进一步检测，而有些则不需要。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="https://www.kaggle.com/c/intel-mobileodt-cervical-cancer-screening/data" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https：//www.kaggle.com/c/intel-mobileodt-cervical-cancer-screening/data</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">多发性硬化症病变分割</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">挑战2008。收集脑部MRI扫描以检测MS病变。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="http://www.ia.unc.edu/MSseg/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.ia.unc.edu/MSseg/</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">多模态脑肿瘤分割挑战</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">脑肿瘤磁共振扫描的大型数据集。自 2012 年以来，他们每年都在扩展这个数据集并发起挑战。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="http://braintumorsegmentation.org/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://braintumorsegmentation.org/</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">癌症编码</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">美国国立卫生研究院基金会和 Sage Bionetworks 发起了一项新计划，旨在举办一系列旨在改善癌症筛查的挑战赛。第一个挑战赛是数字乳房 X 线摄影读数。第二个挑战赛是肺癌检测。这些挑战赛尚未启动。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="http://coding4cancer.org/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://coding4cancer.org/</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Kaggle 上的 EEG 挑战数据集</font></font></strong></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">墨尔本大学 AES/MathWorks/NIH 癫痫发作预测 - 通过长期人类颅内脑电图记录预测癫痫发作</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="https://www.kaggle.com/c/melbourne-university-seizure-prediction" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https：//www.kaggle.com/c/melbourne-university-seizure-prediction</font></font></a></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">美国癫痫协会癫痫发作预测挑战赛 - 通过颅内脑电图记录预测癫痫发作</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="https://www.kaggle.com/c/seizure-prediction" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https：//www.kaggle.com/c/seizure-prediction</font></font></a></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">宾夕法尼亚大学和梅奥诊所的癫痫发作检测挑战赛 - 通过颅内脑电图记录检测癫痫发作</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="https://www.kaggle.com/c/seizure-detection" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https：//www.kaggle.com/c/seizure-detection</font></font></a></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">抓握和举起脑电图检测 - 从脑电图记录中识别手部动作</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="https://www.kaggle.com/c/grasp-and-lift-eeg-detection" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https：//www.kaggle.com/c/grasp-and-lift-eeg-detection</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MICCAI 会议中的挑战轨迹</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医学图像计算和计算机辅助干预。大多数挑战赛都已在 grand-challenges 等网站上介绍。您仍然可以在会议网站的“卫星活动”选项卡下查看所有挑战赛。</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2019 年 - </font></font><a href="https://www.miccai2019.org/programme/workshops-challenges-tutorials/#tablepress-10" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://www.miccai2019.org/programme/workshops-challenges-tutorials/#tablepress-10</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2018 年 - </font></font><a href="https://www.miccai2018.org/en/WORKSHOP---CHALLENGE---TUTORIAL.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://www.miccai2018.org/en/WORKSHOP---CHALLENGE---TUTORIAL.html</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2017 年 - </font></font><a href="http://www.miccai2017.org/satellite-events" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.miccai2017.org/satellite-events</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2016 年 - </font></font><a href="http://www.miccai2016.org/en/SATELLITE-EVENTS.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.miccai2016.org/en/SATELLITE-EVENTS.html</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2015 年 - </font></font><a href="https://www.miccai2015.org/frontend/index.php?page_id=589" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://www.miccai2015.org/frontend/index.php?page_id=589</font></font></a></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="http://www.miccai.org/ConferenceHistory" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.miccai.org/ConferenceHistory</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">国际生物医学成像研讨会（ISBI）</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">IEEE 国际生物医学成像研讨会 (ISBI) 是一场科学会议，致力于探讨生物医学成像的数学、算法和计算方面，涵盖所有观察尺度。大多数挑战都将列在大挑战中。您仍可通过访问每年网站中“计划”下的“挑战”选项卡来访问它。</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2019 年——https: </font></font><a href="https://biomedicalimaging.org/2019/challenges/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">//biomedicalimaging.org/2019/challenges/</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2018 年——https: </font></font><a href="https://biomedicalimaging.org/2018/challenges/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">//biomedicalimaging.org/2018/challenges/</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2017 年——http: </font></font><a href="http://biomedicalimaging.org/2017/challenges/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">//biomedicalimaging.org/2017/challenges/</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2016 年 - </font></font><a href="http://biomedicalimaging.org/2016/?page_id=416" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://biomedicalimaging.org/2016/?page_id=416</font></font></a></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="http://biomedicalimaging.org" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://biomedicalimaging.org</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">持续注册挑战（CRC）</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">连续配准挑战赛 (CRC) 是一项受现代软件开发实践启发的肺部和大脑图像配准挑战赛。参赛者使用开源 SuperElastix C++ API 实现他们的算法。该挑战赛重点关注肺部和大脑的成对配准，这是临床环境中经常遇到的两个问题。他们收集了七个开放访问数据集和一个私有数据集（3+1 个肺部数据集，4 个大脑数据集）。挑战赛结果将在即将举行的生物医学图像配准研讨会 (WBIR 2018) 上展示和讨论。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="https://continuousregistration.grand-challenge.org/home/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https：//continuousregistration.grand-challenge.org/home/</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自动非刚性组织学图像配准 (ANHIR)</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这项 ANHIR 挑战赛旨在对用不同染料染色的组织病理学组织样本的 2D 全切片成像 (WSI) 显微镜图像进行自动非线性图像配准。这项任务非常困难，因为非线性变形会影响组织样本，每种染色的外观不同，纹理重复，而且整个幻灯片图像的尺寸很大。</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">挑战：</font></font><a href="https://anhir.grand-challenge.org/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://anhir.grand-challenge.org/</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基准：</font></font><a href="http://borda.github.io/BIRL" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://borda.github.io/BIRL</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">参考：</font></font><a href="https://www.researchgate.net/publication/338291737_BIRL_Benchmark_on_Image_Registration_methods_with_Landmark_validation" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BIRL：使用 Landmark 验证的图像配准方法基准</font></font></a></li>
</ul>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 MURA 进行骨骼 X 射线深度学习竞赛</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MURA（肌肉骨骼 X 光片）是一个大型骨骼 X 光片数据集。斯坦福 ML 小组和 AIMI 中心正在举办一场比赛，其中算法的任务是确定 X 光片研究是正常还是异常。算法在 207 项肌肉骨骼研究的测试集上进行评估，其中每项研究均由 6 名经委员会认证的放射科医生分别回顾性地标记为正常或异常。其中三名放射科医生用于创建黄金标准，定义为放射科医生标签的多数票，另外三名放射科医生用于获得最佳放射科医生表现，定义为以黄金标准为事实的三名放射科医生的最高分。挑战赛排行榜公开发布，每两周更新一次。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="https://stanfordmlgroup.github.io/competitions/mura/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https：//stanfordmlgroup.github.io/competitions/mura/</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2019 年肾脏和肾脏肿瘤分割挑战赛 (KiTS19)</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">KiTS19 挑战赛的主题是增强 CT 扫描中肾脏和肾脏肿瘤的语义分割。数据集包括 300 名患者的术前动脉期腹部 CT，这些 CT 由专家注释。其中 210 个（70%）作为训练集发布，其余 90 个（30%）作为测试集。该挑战赛与 MICCAI 2019 联合举办。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：</font></font><a href="https://github.com/neheller/kits19/"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https：//github.com/neheller/kits19/</font></font></a></p>
<hr>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3. 来自电子健康记录（EHR）的数据</font></font></h2><a id="user-content-3-data-derived-from-electronic-health-records-ehrs" class="anchor" aria-label="永久链接：3. 来自电子健康记录 (EHR) 的数据" href="#3-data-derived-from-electronic-health-records-ehrs"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从数百万临床叙述中构建医学图表</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
从 1400 万份临床记录和 26 万名患者中提取的医学术语共现统计。</font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
论文：</font></font><a href="http://www.nature.com/articles/sdata201432" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.nature.com/articles/sdata201432</font></font></a><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
数据：</font></font><a href="http://datadryad.org/resource/doi:10.5061/dryad.jp917" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://datadryad.org/resource/doi:10.5061 /dryad.jp917</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">学习医学概念的低维表示</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
使用索赔数据构建医学概念的低维嵌入。请注意，本文利用了</font></font><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从数百万个临床叙述中构建医学图表的</font></font></em><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据
论文：</font></font><a href="http://cs.nyu.edu/~dsontag/papers/ChoiChiuSontag_AMIA_CRI16.pdf" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://cs.nyu.edu/~dsontag/papers/ChoiChiuSontag_AMIA_CRI16.pdf</font></font></a><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
数据： https: </font></font><a href="https://github.com/clinicalml/embeddings"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">//github.com/clinicalml/embeddings</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MIMIC-III，一个可免费访问的重症监护数据库，</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
匿名重症监护 EHR 数据库，包含 38,597 名患者和 53,423 名 ICU 入院患者。</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">需要注册</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
论文：</font></font><a href="http://www.nature.com/articles/sdata201635" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.nature.com/articles/sdata201635</font></font></a><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
数据：</font></font><a href="http://physionet.org/physiobank/database/mimic3cdb/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http ://physionet.org/physiobank/database/mimic3cdb/</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从海量医疗数据源中学习到的临床概念</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
嵌入 从 6000 万名患者、170 万篇期刊文章和 2000 万名患者的临床笔记中学习到的 108,477 个医疗概念嵌入</font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
论文：</font></font><a href="https://arxiv.org/abs/1804.01486" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https</font></font></a><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> ://arxiv.org/abs/1804.01486
嵌入： &nbsp; </font></font><a href="https://figshare.com/s/00d69861786cd0156d81" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://figshare.com/s/00d69861786cd0156d81</font></font></a><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
交互式工具：</font></font><a href="http://cui2vec.dbmi.hms.harvard.edu" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://cui2vec.dbmi.hms.harvard.edu</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">癌症中心患者实验室检测代码嵌入评估</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
200 维 Word2Vec 嵌入，包含 1098 个实验室检测代码 (LOINC)，这些代码是从希望之城国家医疗中心 (加利福尼亚州洛杉矶) 79,081 名患者的 8,280,238 份实验室订单中训练出来的。</font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
论文：</font></font><a href="https://arxiv.org/abs/1907.09600" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://arxiv.org/abs/1907.09600</font></font></a><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
嵌入和代码：</font></font><a href="https://github.com/elleros/DSHealth2019_loinc_embeddings"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/elleros/DSHealth2019_loinc_embeddings</font></font></a></p>
<hr>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">4. 国家医疗保健数据</font></font></h2><a id="user-content-4-national-healthcare-data" class="anchor" aria-label="永久链接：4. 国家医疗保健数据" href="#4-national-healthcare-data"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">美国疾病控制与预防中心 (CDC)</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> 
CDC 提供的涉及许多领域的数据，包括：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">生物监测</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">儿童疫苗接种</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">流感疫苗</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">健康统计</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">伤害与暴力</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">最大重复次数</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">摩托车</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">国家卫生服务中心</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">国家国防安全决策支持系统</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">怀孕和接种疫苗</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">性传播疾病</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">吸烟与烟草使用</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">青少年疫苗接种</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">创伤性脑损伤</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">疫苗接种</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">网络指标</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">登陆页面：</font></font><a href="https://data.cdc.gov" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://data.cdc.gov</font></font></a><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
数据目录：</font></font><a href="https://data.cdc.gov/browse" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://data.cdc.gov/browse</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医疗保险数据</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
来自医疗保险和医疗补助服务中心 (CMS) 的医院、疗养院、医生、家庭医疗保健、透析和设备提供商的数据。</font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
登录页面：</font></font><a href="https://data.medicare.gov" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://data.medicare.gov</font></font></a><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> 
Explorer：</font></font><a href="https://data.medicare.gov/data" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://data.medicare.gov/data</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">德克萨斯州公共用途住院患者数据文件，</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
其中包含 2006 年至 2009 年间德克萨斯州 1100 万住院患者的诊断、手术代码和结果的数据。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">链接：</font></font><a href="https://www.dshs.texas.gov/thcic/hospitals/Inpatientpudf.shtm" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://www.dshs.texas.gov/thcic/hospitals/Inpatientpudf.shtm</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医生的钱</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
Propublica 调查了制药公司支付给医生的钱。</font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
信息：</font></font><a href="https://www.propublica.org/series/dollars-for-docs" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://www.propublica.org/series/dollars-for-docs</font></font></a><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
搜索工具：</font></font><a href="https://projects.propublica.org/docdollars/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://projects.propublica.org/docdollars/</font></font></a><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
数据请求：</font></font><a href="https://projects.propublica.org/data-store/sets/health-d4d-national-2" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://projects.propublica.org/data-store/sets/health-d4d-national-2</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DocGraph</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
通过信息自由法案请求获得的医生互动网络。涵盖近 100 万个实体。</font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
主页：</font></font><a href="http://www.docgraph.com" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.docgraph.com</font></font></a><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
信息：</font></font><a href="http://thehealthcareblog.com/blog/2012/11/05/tracking-the-social-doctor-opening-up-physician-referral-data-and-much-more/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://thehealthcareblog.com/blog/2012/11/05/tracking-the-social-doctor-opening-up-physician-referral-data-and-much-more/</font></font></a><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
数据：</font></font><a href="http://linea.docgraph.org" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http ://linea.docgraph.org</font></font></a></p>
<hr>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">5.UCI 数据集</font></font></h2><a id="user-content-5-uci-datasets" class="anchor" aria-label="永久链接：5. UCI 数据集" href="#5-uci-datasets"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">肝脏疾病数据集</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
345 名患有和不患有肝病的患者的数据。特征是 5 种被认为与肝病有关的血液生物标志物。</font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
数据：</font></font><a href="https://archive.ics.uci.edu/ml/datasets/Liver+Disorders" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://archive.ics.uci.edu/ml/datasets/Liver+Disorders</font></font></a></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">甲状腺疾病数据集</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
数据：</font></font><a href="https://archive.ics.uci.edu/ml/datasets/Thyroid+Disease" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://archive.ics.uci.edu/ml/datasets/Thyroid+Disease</font></font></a></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">乳腺癌数据集</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
数据：</font></font><a href="https://archive.ics.uci.edu/ml/datasets/Breast+Cancer" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://archive.ics.uci.edu/ml/datasets/Breast+Cancer</font></font></a></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">心脏病数据集</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
数据：</font></font><a href="https://archive.ics.uci.edu/ml/datasets/Heart+Disease" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://archive.ics.uci.edu/ml/datasets/Heart+Disease</font></font></a></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">淋巴造影数据集</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
数据：</font></font><a href="https://archive.ics.uci.edu/ml/datasets/Lymphography" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://archive.ics.uci.edu/ml/datasets/Lymphography</font></font></a></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">帕金森数据集</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
数据：</font></font><a href="https://archive.ics.uci.edu/ml/datasets/parkinsons" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://archive.ics.uci.edu/ml/datasets/parkinsons</font></font></a></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">帕金森远程监测数据集</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
数据：</font></font><a href="https://archive.ics.uci.edu/ml/datasets/Parkinsons+Telemonitoring" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://archive.ics.uci.edu/ml/datasets/Parkinsons+Telemonitoring</font></font></a></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">包含多种类型声音记录的帕金森语音数据集 数&ZeroWidthSpace;&ZeroWidthSpace;据集</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
数据：</font></font><a href="https://archive.ics.uci.edu/ml/datasets/Parkinson+Speech+Dataset+with++Multiple+Types+of+Sound+Recordings" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://archive.ics.uci.edu/ml/datasets/Parkinson+Speech+Dataset+with++Multiple+Types+of+Sound+Recordings</font></font></a></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">帕金森病分类数据集</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
数据：</font></font><a href="https://archive.ics.uci.edu/ml/datasets/Parkinson%27s+Disease+Classification" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://archive.ics.uci.edu/ml/datasets/Parkinson%27s+Disease+Classification</font></font></a></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">原发性肿瘤数据集</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
数据：</font></font><a href="https://archive.ics.uci.edu/ml/datasets/primary+tumor" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://archive.ics.uci.edu/ml/datasets/primary+tumor</font></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">6. 生物医学文献</font></font></h2><a id="user-content-6-biomedical-literature" class="anchor" aria-label="永久链接：6. 生物医学文献" href="#6-biomedical-literature"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PMC 开放存取子集</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
收集 Pubmed central 中的所有全文、开放存取文章。</font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
信息：</font></font><a href="http://www.ncbi.nlm.nih.gov/pmc/tools/openftlist/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.ncbi.nlm.nih.gov/pmc/tools/openftlist/</font></font></a><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
存档文件：</font></font><a href="http://www.ncbi.nlm.nih.gov/pmc/tools/ftp/#Data_Mining" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.ncbi.nlm.nih.gov/pmc/tools/ftp/#Data_Mining</font></font></a></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PubMed 200k RCT</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">随机对照试验 (RCT) 的 pubmed 摘要集合。摘要中每句话均有注释。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">论文：</font></font><a href="https://arxiv.org/abs/1710.06071" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://arxiv.org/abs/1710.06071</font></font></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据：</font></font><a href="https://github.com/Franck-Dernoncourt/pubmed-rct"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/Franck-Dernoncourt/pubmed-rct</font></font></a></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PubMed 文章的 Web API</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">NLM还提供了用于访问PubMed中的生物医学文献的Web API。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">获取 PubMed 文章的说明：</font></font><a href="https://www.ncbi.nlm.nih.gov/research/bionlp/APIs/BioC-PubMed/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://www.ncbi.nlm.nih.gov/research/bionlp/APIs/BioC-PubMed/</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（不是全文，只有标题、摘要等）</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对于 PubMed Central 中的文章，获取完整文章的说明：</font></font><a href="https://www.ncbi.nlm.nih.gov/research/bionlp/APIs/BioC-PMC/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://www.ncbi.nlm.nih.gov/research/bionlp/APIs/BioC-PMC/</font></font></a></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">循证医学自然语言处理</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">收集随机对照试验 (RCT) 的 pubmed 摘要。提供人群、干预和结果 (PICO 元素) 注释。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">论文：</font></font><a href="https://arxiv.org/abs/1806.04185" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://arxiv.org/abs/1806.04185</font></font></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据：</font></font><a href="https://ebm-nlp.herokuapp.com/annotations" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://ebm-nlp.herokuapp.com/annotations</font></font></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">网址：</font></font><a href="https://ebm-nlp.herokuapp.com/index" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://ebm-nlp.herokuapp.com/index</font></font></a></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">证据推理</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于推断随机对照试验 (RCT) 结果的数据集。来自开放获取子集的 pubmed RCT 集合。提供（干预、比较干预、结果、重要性发现、证据跨度）注释。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">论文：</font></font><a href="https://arxiv.org/abs/1904.01606" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://arxiv.org/abs/1904.01606</font></font></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据：</font></font><a href="https://github.com/jayded/evidence-inference/tree/master/annotations"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https：//github.com/jayded/evidence-inference/tree/master/annotations</font></font></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">网址：</font></font><a href="http://evidence-inference.ebm-nlp.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://evidence-inference.ebm-nlp.com/</font></font></a></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PubMed问答</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">生物医学研究问答数据集。任务是使用是/否/可能来回答 PubMed 标题中自然出现的问题。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">论文：</font></font><a href="https://arxiv.org/abs/1909.06146" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://arxiv.org/abs/1909.06146</font></font></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据：</font></font><a href="https://github.com/pubmedqa/pubmedqa"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/pubmedqa/pubmedqa</font></font></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">网址：</font></font><a href="https://pubmedqa.github.io/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://pubmedqa.github.io/</font></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">6. TREC 精准医学/临床决策支持轨道</font></font></h2><a id="user-content-6-trec-precision-medicine--clinical-decision-support-track" class="anchor" aria-label="永久链接：6. TREC 精准医学/临床决策支持轨道" href="#6-trec-precision-medicine--clinical-decision-support-track"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文本检索会议 (TREC) 从 2014 年起开始举办精准医疗/临床决策支持主题的研讨会。</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2014 年临床决策支持跟踪</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
重点：检索与回答有关医疗记录的一般临床问题相关的生物医学文章。</font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
信息和数据：</font></font><a href="http://www.trec-cds.org/2014.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.trec-cds.org/2014.html</font></font></a></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2015 临床决策支持跟踪</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
重点：检索与回答有关医疗记录的一般临床问题相关的生物医学文章。</font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
信息和数据：</font></font><a href="http://www.trec-cds.org/2015.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.trec-cds.org/2015.html</font></font></a></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2016 年临床决策支持跟踪</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
重点：检索与回答有关医疗记录的一般临床问题相关的生物医学文章。实际的电子健康记录 (EHR) 患者记录将代替合成病例。</font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
信息和数据：</font></font><a href="http://www.trec-cds.org/2016.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.trec-cds.org/2016.html</font></font></a></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2017 年临床决策支持跟踪</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
重点：为治疗癌症患者的临床医生检索有用的精准医疗相关信息。</font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
信息和数据：</font></font><a href="http://www.trec-cds.org/2017.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.trec-cds.org/2017.html</font></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">7. 医疗语音数据</font></font></h2><a id="user-content-7-medical-speech-data" class="anchor" aria-label="永久链接：7. 医学语音数据" href="#7-medical-speech-data"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TORGO 数据库：患有构音</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
障碍的说话者的声学和发音 TORGO 构音障碍数据库包括对齐的声学和测量的 3D 发音特征，这些特征来自患有脑瘫 (CP) 或肌萎缩侧索硬化症 (ALS) 的说话者，这两种疾病是导致言语障碍的最常见原因（Kent 和 Rosen，2004 年），以及匹配的对照者。这个名为 TORGO 的数据库是多伦多大学计算机科学和言语病理学系与多伦多 Holland-Bloorview 儿童康复医院合作的成果。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">信息和数据：</font></font><a href="http://www.cs.toronto.edu/~complingweb/data/TORGO/torgo.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.cs.toronto.edu/~complingweb/data/TORGO/torgo.html</font></font></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">论文：</font></font><a href="https://www.researchgate.net/publication/225446742_The_TORGO_database_of_acoustic_and_articulatory_speech_from_speakers_with_dysarthria" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">链接</font></font></a></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">NKI-CCRT 语料库：晚期头颈癌患者接受同期放化疗治疗前后的语音清晰度。NKI-</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> 
CCRT 语料库包含听众对 55 名接受头颈癌治疗的说话者录音清晰度的个人判断，将仅供有限制的科学使用。该语料库包含三个评估时刻的语音清晰度录音和感知评估：治疗前和治疗后（10 周和 12 个月）。治疗采用放化疗 (CCRT)。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">论文：</font></font><a href="http://lrec.elra.info/proceedings/lrec2012/pdf/230_Paper.pdf" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://lrec.elra.info/proceedings/lrec2012/pdf/230_Paper.pdf</font></font></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问：联系作者。</font></font></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">非典型情感跨语言子挑战</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Björn Schuller、Simone Hantke 及其同事正在提供 EMOTASS 语料库。这个独特的语料库首次提供了残疾人情感语音录音，涵盖了更广泛的精神、神经和身体残疾。它包括 15 名残疾成年人的录音（年龄范围从 19 岁到 58 岁，平均年龄为 31.6 岁）。任务是从他们面对非典型表现的言语中对五种情绪进行分类。录音是在他们日常工作环境中进行的。总体而言，其中包括约 11000 条话语和约 9 小时的语音。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">论文：</font></font><a href="http://emotion-research.net/sigs/speech-sig/is2018_compare.pdf" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://emotion-research.net/sigs/speech-sig/is2018_compare.pdf</font></font></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">链接：</font></font><a href="http://emotion-research.net/sigs/speech-sig/is18-compare" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://emotion-research.net/sigs/speech-sig/is18-compare</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<hr>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自闭症子挑战</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自闭症子挑战赛基于“儿童病理性语言数据库”（CPSD）。它提供了法国巴黎两所大学儿童和青少年精神病学系（皮埃尔和玛丽居里大学/Pitie Salpetiere 医院和勒内笛卡尔大学/Necker 医院）录制的语音。子挑战赛中使用的数据集包含来自 99 名 6 至 18 岁儿童的 2.5k 个语音记录实例</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">论文：</font></font><a href="http://emotion-research.net/sigs/speech-sig/is2013_compare.pdf" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://emotion-research.net/sigs/speech-sig/is2013_compare.pdf</font></font></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">链接：</font></font><a href="http://emotion-research.net/sigs/speech-sig/is13-compare" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://emotion-research.net/sigs/speech-sig/is13-compare</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
</article></div>
